#!/bin/bash -eux

# Links
# https://hub.docker.com/r/codercom/code-server/tags?page=1&ordering=last_updated
# https://github.com/cdr/code-server
# https://github.com/cdr/code-server/blob/c4610f7829701aadb045d450013b84491c30580d/ci/release-image/Dockerfile
# https://github.com/cdr/code-server/blob/v3.8.0/doc/FAQ.md

if [ "$(docker ps -q -f name=vscodeserver)" ]; then
    echo "code-server is running"
elif [ "$(docker ps -aq -f status=exited -f name=vscodeserver)" ]; then
    docker start vscodeserver
else
    docker run -it -p 127.0.0.1:8080:8080 -v "$HOME/.config:/home/coder/.config" -v "$1:/home/coder/workspace" -u "$(id -u):$(id -g)" --name vscodeserver codercom/code-server:3.8.0
fi

# getting the password
# we can also set the password by editing the password field at code-server config file
cat ~/.config/code-server/config.yaml